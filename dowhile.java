package alpro2_uts;

import java.util.Scanner;

public class dowhile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char pilihan = 'y';

        do {
            System.out.print("Masukkan angka pertama: ");
            double angka1 = input.nextDouble();

            System.out.print("Masukkan operasi Aritmatika(+ * / -): ");
            char operator = input.next().charAt(0);

            System.out.print("Masukkan angka kedua: ");
            double angka2 = input.nextDouble();

            double hasil;

            if (operator == '+') {
                hasil = angka1 + angka2;
                System.out.println(angka1 + " + " + angka2 + " = " + hasil);
            } else if (operator == '-') {
                hasil = angka1 - angka2;
                System.out.println(angka1 + " - " + angka2 + " = " + hasil);
            } else if (operator == '*') {
                hasil = angka1 * angka2;
                System.out.println(angka1 + " * " + angka2 + " = " + hasil);
            } else if (operator == '/') {
                if (angka2 != 0) {
                    hasil = angka1 / angka2;
                    System.out.println(angka1 + " / " + angka2 + " = " + hasil);
                } else {
                    System.out.println("Pembagian dengan nol tidak diperbolehkan.");
                    break;
                }
            } else {
                System.out.println("Operasi tidak valid.");
                break;
            }
            System.out.println("Apakah anda ingin menggunakannya lagi?(y/n)");
            pilihan = input.next().charAt(0);
        } while (pilihan == 'y' || pilihan == 'Y');
        System.out.println("Terima Kasih telah menggunakan Kalkulator ini");
    }
}
