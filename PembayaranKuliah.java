package alpro2_uts;

import java.util.Scanner;

public class PembayaranKuliah {
    private int biayaKuliah;
    private int tarifDenda;

    public PembayaranKuliah(int biayaKuliah, int tarifDenda) {
        this.biayaKuliah = biayaKuliah;
        this.tarifDenda = tarifDenda;
    }

    // Getter dan Setter
    public int getBiayaKuliah() {
        return biayaKuliah;
    }

    public void setBiayaKuliah(int biayaKuliah) {
        this.biayaKuliah = biayaKuliah;
    }

    public int getTarifDenda() {
        return tarifDenda;
    }

    public void setTarifDenda(int tarifDenda) {
        this.tarifDenda = tarifDenda;
    }

    public int hitungTotalPembayaran(boolean terlambat) {
        int totalPembayaran = terlambat ? biayaKuliah + (biayaKuliah * tarifDenda) : biayaKuliah;
        return totalPembayaran;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan biaya kuliah: ");
        int biayaKuliah = scanner.nextInt();

        System.out.print("Masukkan tarif denda (dalam desimal): ");
        int tarifDenda = scanner.nextInt();

        PembayaranKuliah kalkulator = new PembayaranKuliah(biayaKuliah, tarifDenda);

        System.out.print("Apakah Anda terlambat membayar (true/false)? ");
        boolean terlambat = scanner.nextBoolean();

        int totalPembayaran = kalkulator.hitungTotalPembayaran(terlambat);
        System.out.println("Total pembayaran: " + totalPembayaran);

        scanner.close();
    }
}
