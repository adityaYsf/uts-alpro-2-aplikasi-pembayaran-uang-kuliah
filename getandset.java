package alpro2_uts;

public class getandset {
    private double angka1;
    private double angka2;
    private char operator;

    // Constructor
    public getandset() {
        angka1 = 0;
        angka2 = 0;
        operator = '+';
    }

    public double getangka1() {
        return angka1;
    }

    public void setangka1(double angka1) {
        this.angka1 = angka1;
    }

    public double getangka2() {
        return angka2;
    }

    public void setangka2(double angka2) {
        this.angka2 = angka2;
    }

    public char getOperator() {
        return operator;
    }

    public void setOperator(char operator) {
        this.operator = operator;
    }

    public double hitung() {
        double hasil = 0;
        switch (operator) {
            case '+':
                hasil = angka1 + angka2;
                break;
            case '-':
                hasil = angka1 - angka2;
                break;
            case '*':
                hasil = angka1 * angka2;
                break;
            case '/':
                if (angka2 != 0) {
                    hasil = angka1 / angka2;
                } else {
                    System.out.println("Error: Cannot divide by zero");
                }
                break;
            default:
                System.out.println("Error: Invalid operator");
        }
        return hasil;
    }

    public static void main(String[] args) {
        getandset kalkulator = new getandset();
        kalkulator.setangka1(10);
        kalkulator.setangka2(5);
        kalkulator.setOperator('+');
        System.out.println("Hasil: " + kalkulator.hitung());
    }
}
